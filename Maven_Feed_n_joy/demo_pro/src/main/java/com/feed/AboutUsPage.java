// Author : Monika Bhosale 

package com.feed;
import javafx.application.Application;
import java.io.FileInputStream;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class AboutUsPage extends Application {
    @Override
    public void start(Stage prStage) throws IOException {
        prStage.setTitle("AboutUs Page");
        
        Screen screen = Screen.getPrimary();
        double screenWidth = screen.getBounds().getWidth();
        double screenHeight = screen.getBounds().getHeight();

    // Set the stage size to the screen size
        prStage.setWidth(screenWidth);
        prStage.setHeight(screenHeight);
        
    // set background image 
        FileInputStream fxd_backImage=new FileInputStream("demo_pro\\src\\main\\resources\\Assets\\ba.jpg");
        Image fxd_bc=new Image(fxd_backImage);
        ImageView fxd_iv=new ImageView(fxd_bc);
        fxd_iv.setFitWidth(2000);
        fxd_iv.setFitHeight(1300);

    // Buttons for navbar
        Button fxd_homeButton = createStyledButton("Home");
        Button fxd_aboutButton = createStyledButton("About");
        Button fxd_contactButton = createStyledButton("Contact");
        Button fxd_registerButton = createStyledButton("Register");
        Button fxd_storiesButton = createStyledButton("Stories");
    
    //Image of logo
        FileInputStream fxd_lo=new FileInputStream("demo_pro\\src\\main\\resources\\Assets\\Images_Abtus\\hlogo.png");
        Image fxd_logo=new Image(fxd_lo);
        ImageView fxd_logov= new ImageView(fxd_logo);
        fxd_logov.setFitHeight(170);
        fxd_logov.setFitWidth(400);

    // Hbox for navbar    
        HBox fxd_navbar = new HBox(fxd_homeButton, fxd_aboutButton, fxd_registerButton, fxd_contactButton,fxd_storiesButton);
        fxd_navbar.setSpacing(50);
        fxd_navbar.setPrefHeight(100);
        fxd_navbar.setAlignment(Pos.CENTER_RIGHT);
        
    //hbox for logo+navbar
    HBox fxd_hbox1_nav=new HBox(600,fxd_logov,fxd_navbar);

    Label fxd_content = new Label("");

    // display inner information of button
    fxd_homeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override   
            public void handle(ActionEvent event) {
                fxd_content.setText("homepage");
            }    
        });
        fxd_aboutButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                fxd_content.setText(" About page");
            }    
        });
        fxd_registerButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                fxd_content.setText(" Register as");
            }    
        });
        fxd_contactButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                fxd_content.setText("contact us");
            }    
        });
        fxd_storiesButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                fxd_content.setText(" stories");
            }    
        });

    // Mission Label    
        Label fxd_lb1=new Label("Our Mission");
        fxd_lb1.setFont(Font.font("Arial", FontWeight.BOLD, 35)); 
        fxd_lb1.setTextFill(Color.WHITE);
        fxd_lb1.setAlignment(Pos.TOP_LEFT);

    //Mission Content    
        Label fxd_lb2=new Label("At Feed-n-joy, our mission is to create a sustainable \nfuture by reducing food waste and providing nutri- \ntious meals to those in need. We strive  to bridge \nthe gap between food surplus & food scarcity through \ninnovative solutions, community engagement,& strategic \npartnerships. By rescuing surplus food & redistributing \nit to hungry individuals and families,we aim to promote\nenvironmental sustainability, social responsibility,&food \nsecurity for all.\n" );
        fxd_lb2.setFont(Font.font("Arial", FontWeight.SEMI_BOLD, 22)); 
        fxd_lb2.setTextFill(Color.WHITE);
        fxd_lb2.setLayoutX(10);
        fxd_lb2.setLayoutY(140); 
    //Mission Image
        FileInputStream fxd_miss =new FileInputStream("demo_pro\\src\\main\\resources\\Assets\\Images_Abtus\\about.png");
        Image fxd_ig=new Image(fxd_miss);
        ImageView fxd_im=new ImageView(fxd_ig);
        fxd_im.setPreserveRatio(true);
        fxd_im.setFitWidth(500);
        fxd_im.setFitHeight(250);
    //add mission content and image in HBox
        HBox fxd_hb1=new HBox(30,fxd_lb2,fxd_im);
        fxd_hb1.setLayoutX(30);
    // Mission Label and HBox(mission) in VBox
        VBox fxd_mission=new VBox(20,fxd_lb1,fxd_hb1);
        fxd_mission.setLayoutX(30);
        fxd_mission.setLayoutY(30);
        fxd_mission.setStyle("-fx-border-color:LIGHTBLUE; -fx-border-width: 2; -fx-border-radius: 5; -fx-padding: 30");
    
    // Label  of Vision    
        Label fxd_ll1=new Label("Our Vision");
        fxd_ll1.setFont(Font.font("Arial", FontWeight.BOLD, 35)); 
        fxd_ll1.setTextFill(Color.WHITE);
        fxd_ll1.setAlignment(Pos.TOP_LEFT);
    //Image of vision 
        FileInputStream fxd_vis =new FileInputStream("demo_pro\\src\\main\\resources\\Assets\\Images_Abtus\\f.png");
        Image fxd_ig1=new Image(fxd_vis);
        ImageView fxd_im1=new ImageView(fxd_ig1);
        fxd_im1.setPreserveRatio(true);
        fxd_im1.setFitWidth(500);
        fxd_im1.setFitHeight(250);
    //Content of vision
        Label fxd_ll2=new Label("Feed-n-joy is an organisation which aim to redistribute \nfood from weddings, parties, events to those who are hungry.. \nAt Feed-n-joy, we envision a world where no one goes \nhungry and food waste is a thing of the past.\nOur goal is to create a sustainable,efficient,& compassionate \nfood management and donation network  that connects \nsurplus food with those in need,fostering a community of \ngenerosity, environmental stewardship,& social responsibility.");
        fxd_ll2.setFont(Font.font("Arial", FontWeight.SEMI_BOLD, 22)); 
        fxd_ll2.setTextFill(Color.WHITE);
    //add content of vision and image in HBox
        HBox fxd_hb2=new HBox(50,fxd_ll2,fxd_im1);
    //add Label of vision and HBox(vision) in one VBox 
        VBox fxd_vision=new VBox(30,fxd_ll1,fxd_hb2);
        fxd_vision.setStyle("-fx-border-color:LIGHTBLUE; -fx-border-width: 2; -fx-border-radius: 5; -fx-padding: 30");
     
    //both mission and vision VBox gives to HBox    
        HBox fxd_middle=new HBox(50,fxd_mission,fxd_vision);
        fxd_middle.setPadding(new Insets(30));
         
    //Label of Our Team 
      Label fxd_Team=new Label("Our Team");
      fxd_Team.setFont(Font.font("Arial", FontWeight.BOLD, 40)); 
      fxd_Team.setTextFill(Color.WHITE);
      fxd_Team.setAlignment(Pos.CENTER);
    //Image of Team Member (Anuj Bankar)
        FileInputStream fxd_anuj =new FileInputStream("demo_pro\\src\\main\\resources\\Assets\\Images_Abtus\\anuj.jpg");
        Image fxd_member1=new Image(fxd_anuj);
        ImageView fxd_m1=new ImageView(fxd_member1);
        fxd_m1.setPreserveRatio(true);
        fxd_m1.setFitWidth(500);
        fxd_m1.setFitHeight(250);
        Text fxd_tx=new Text("        Anuj Bankar");
        fxd_tx.setFill(Color.WHITE);
        fxd_tx.setFont(Font.font("Arial", FontWeight.BOLD, 25)); 
        Text fxd_tx1=new Text("       Programmer");
        fxd_tx1.setFill(Color.WHITE);
        fxd_tx1.setFont(Font.font("Arial", FontWeight.SEMI_BOLD, 25)); 
    //Image and Information about Anuj in VBox
        VBox fxd_Team1=new VBox(15,fxd_m1,fxd_tx,fxd_tx1);
        fxd_Team1.setStyle("-fx-border-color:LIGHTBLUE; -fx-border-width: 2; -fx-border-radius: 5; -fx-padding: 25");

    //Images of Team Member (Varun Deshmukh)
        FileInputStream fxd_var =new FileInputStream("demo_pro\\src\\main\\resources\\Assets\\Images_Abtus\\varun.jpg");
        Image fxd_member2=new Image(fxd_var);
        ImageView fxd_m2=new ImageView(fxd_member2);
        fxd_m2.setPreserveRatio(true);
        fxd_m2.setFitWidth(500);
        fxd_m2.setFitHeight(250);
        Text fxd_txtG=new Text("        Varun Deshmukh");
        fxd_txtG.setFill(Color.WHITE);
        fxd_txtG.setFont(Font.font("Arial", FontWeight.BOLD, 25)); 
        Text fxd_txtG1=new Text("       Programmer");
        fxd_txtG1.setFill(Color.WHITE);
        fxd_txtG1.setFont(Font.font("Arial", FontWeight.SEMI_BOLD, 25)); 
    //Image and Information about Gauri in VBox
        VBox fxd_Team3=new VBox(15,fxd_m2,fxd_txtG,fxd_txtG1);    
        fxd_Team3.setStyle("-fx-border-color: LIGHTBLUE; -fx-border-width: 2; -fx-border-radius: 5; -fx-padding: 25");

    //Images of Team Member (Monika Bhosale) 
        FileInputStream fxd_mon=new FileInputStream( "demo_pro\\src\\main\\resources\\Assets\\Images_Abtus\\my.jpg");    
        Image fxd_member3=new Image(fxd_mon);
        ImageView fxd_m3=new ImageView(fxd_member3);
        fxd_m3.setPreserveRatio(true);
        fxd_m3.setFitWidth(500);
        fxd_m3.setFitHeight(250);
        Text txt=new Text("        Monika Bhosale");
        txt.setFill(Color.WHITE);
        txt.setFont(Font.font("Arial", FontWeight.BOLD, 25)); 
        Text txt1=new Text("       Programmer");
        txt1.setFill(Color.WHITE);
        txt1.setFont(Font.font("Arial", FontWeight.SEMI_BOLD, 25)); 
    //Image and Information about monika in VBox
        VBox fxd_Team2=new VBox(15,fxd_m3,txt,txt1);
        fxd_Team2.setStyle("-fx-border-color: LIGHTBLUE; -fx-border-width: 2; -fx-border-radius: 5;  -fx-padding: 25");

    // Image of Team Member (gauri dagale)
        FileInputStream fxd_ga =new FileInputStream("demo_pro\\src\\main\\resources\\Assets\\Images_Abtus\\gaur.jpg");
        Image fxd_mo4=new Image(fxd_ga);
        ImageView fxd_m4=new ImageView(fxd_mo4);
        fxd_m4.setPreserveRatio(true);
        fxd_m4.setFitWidth(500);
        fxd_m4.setFitHeight(250);
        Text fxd_txtV=new Text("        Gauri Dagale");
        fxd_txtV.setFill(Color.WHITE);
        fxd_txtV.setFont(Font.font("Arial", FontWeight.BOLD, 25)); 
        Text fxd_txtV1=new Text("       Programmer");
        fxd_txtV1.setFill(Color.WHITE);
        fxd_txtV1.setFont(Font.font("Arial", FontWeight.SEMI_BOLD, 25));
    //Image and Information about Varun in VBox    
        VBox fxd_Team4=new VBox(15,fxd_m4,fxd_txtV,fxd_txtV1);
        fxd_Team4.setStyle("-fx-border-color: LIGHTBLUE; -fx-border-width: 2; -fx-border-radius: 5; -fx-padding: 25");

    //Image  of core2web
        FileInputStream fxd_cr =new FileInputStream("demo_pro\\src\\main\\resources\\Assets\\Images_Abtus\\c2w.jpg");
        Image fxd_mo5=new Image(fxd_cr);
        ImageView fxd_m5=new ImageView(fxd_mo5);
        fxd_m5.setPreserveRatio(true);
        fxd_m5.setFitWidth(500);
        fxd_m5.setFitHeight(250);
        Text fxd_txtC=new Text("        Core2web ");
        fxd_txtC.setFill(Color.WHITE);
        fxd_txtC.setFont(Font.font("Arial", FontWeight.BOLD, 25)); 
        Text fxd_txtC1=new Text("        Inspiration");
        fxd_txtC1.setFill(Color.WHITE);
        fxd_txtC1.setFont(Font.font("Arial", FontWeight.SEMI_BOLD, 25));
    //Image and Information about Core2web in VBox    
        VBox fxd_Team5=new VBox(15,fxd_m5,fxd_txtC,fxd_txtC1);
        fxd_Team5.setStyle("-fx-border-color:LIGHTBLUE; -fx-border-width: 3; -fx-border-radius: 10;  -fx-padding: 25");

    //All Team Members VBOX in HBox    
        HBox fxd_Next=new HBox(100,fxd_Team1,fxd_Team3,fxd_Team5,fxd_Team2,fxd_Team4);
        fxd_Next.setPadding(new Insets(30));

    //Team Label and HBox in VBox 
        VBox fxd_Leader=new VBox(20,fxd_Team,fxd_Next);
        fxd_Leader.setPadding(new Insets(30));
        fxd_Leader.setAlignment(Pos.CENTER);

    //BorderPane for set Navbar at top    
        BorderPane fxd_Bp=new BorderPane();
        fxd_Bp.setTop(fxd_hbox1_nav);

    // navbar,(mission,vission),Our Team in VBox
        VBox fxd_all=new VBox(20,fxd_Bp,fxd_middle,fxd_Leader);

    // footer content
        Label footerLabel = new Label("© 2024 Feed-N-Joy. All rights reserved.");
        footerLabel.setStyle("-fx-font-size: 20px; -fx-text-fill:BLACK;");
        HBox footer = new HBox(footerLabel);
        footer.setStyle("-fx-background-color:GREY; -fx-padding: 20px;");
        footer.setAlignment(Pos.CENTER);

    //BorderPane for footer
        BorderPane fxd_brFooter = new BorderPane();
        fxd_brFooter.setBottom(footer);
    
    //give background image and all content to group
        StackPane fxd_gr= new  StackPane(fxd_iv,fxd_all,fxd_brFooter);
        ScrollPane fxd_scroll = new ScrollPane(fxd_gr);
        Scene scene = new Scene(fxd_scroll,2000,1200);
        prStage.setScene(scene);
        prStage.show();
    }
    // button style 
        public Button createStyledButton(String text) {
            Button button = new Button(text);
            button.setPrefHeight(50);
            button.setPrefWidth(120);
            button.setStyle("-fx-background-color:GREY; -fx-text-fill: white; -fx-font-size: 20px; -fx-font-weight: bold; -fx-background-radius: 35;");
            button.setOnMouseEntered(e -> button.setStyle("-fx-background-color:BLACK; -fx-text-fill: white; -fx-font-size: 20px; -fx-font-weight: bold; -fx-background-radius: 20;"));
            button.setOnMouseExited(e -> button.setStyle("-fx-background-color:GREY; -fx-text-fill: white; -fx-font-size: 20px; -fx-font-weight: bold; -fx-background-radius: 20;"));
            return button;
    }
}
    

